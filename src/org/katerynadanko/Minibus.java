package org.katerynadanko;

public class Minibus extends Auto{

    int size = 4;
    int capacity = 2;
    int timeLoading = 2;
    int timeUnloading = 4;

    public Minibus(String type, int number) {
        super(type, number);
    }
}
