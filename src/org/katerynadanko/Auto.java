package org.katerynadanko;

public abstract class Auto implements Runnable {

     private String type;
     private int number;

     private int size;
     private int capacity;
     private int timeLoading;
     private int timeUnloading;

     public int getSize() {
          return size;
     }
     public Auto(String type, int number) {
          this.type = type;
          this.number = number;
          this.size = size;
          this.capacity = capacity;
          this.timeLoading = timeLoading;
          this.timeUnloading = timeUnloading;
     }
     public int getCapcity() {
          return capacity;
     }

     public int getTimeLoading() {
          return timeLoading;
     }

     public int getTimeUnloading() {
          return timeUnloading;
     }

     public String toString() {
          return
                  "type='" + type + '\'' +
                  ", number=" + number+" ";
     }
     @Override
     public void run() {

     }
}
