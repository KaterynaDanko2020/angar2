package org.katerynadanko;

import java.util.Date;
import java.util.concurrent.Semaphore;

public class Angar implements GateWaite{
    private Semaphore semaphore;
    private int size;
    private volatile int boxes;

    public Angar(int size) {
        this.size = size;
        this.semaphore = new Semaphore(size, true);
    }
    public int getSize() {
        return size;
    }

    public int getBoxes() {
            return boxes;
    }
    public int increment() {
            return boxes=+5;
    }
    public int decremernt() {
            return boxes--;
    }

    @Override
    public int loadingAuto(Auto auto) {
        Date date = new Date();
        try {
            semaphore.acquire(auto.getSize());
        } catch (Exception e) {
            System.out.println("Нет места! Ждем!");

        }
        System.out.println("Автомобиль "+auto.toString()+date+" въехал в ангар! " +
                "Занял места "+auto.getSize());
        try {
            Thread.sleep(auto.getTimeLoading());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        semaphore.release(auto.getCapcity());
        System.out.println("Автомобиль "+auto.toString()+date+" выехал из ангара!");
        try {
            Thread.sleep(auto.getTimeUnloading());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return getSize();
    }

}
